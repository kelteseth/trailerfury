using Godot;
using System;

public class enemyController : VehicleBody
{
    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";

    [Export]
    public float STEER_SPEED = 1.5f;
    [Export]
    public float STEER_LIMIT = 0.8f;
    [Export]
    public float engine_force_value = 40.0f;
    [Export]
    public float health = 100.0f;
	/*
	[Export]
    public PackedScene wheel_broken; // DOES NOT WORK, HACKED TOGETHER INSTEAD
	*/

    protected Spatial player;
    protected float steer_angle = 0.0f;
    protected float steer_target = 0.0f;
    protected float stunDuration = 0.0f;
	
	private SpotLight spotlight;


    private Vector3 lastAngularVelocity;
    private Vector3 lastLinearVelocity;
    private bool applyDamage = false;
    private Particles damageParticleEffect;
	private Score score;
	
	public void Explode()
	{
		this.score.DecreaseEnemyCount();
		
        PackedScene wheel_broken = (PackedScene)ResourceLoader.Load("res://prefabs/MonsterTruckWheel_broken.tscn");
        
		var childs = GetChildren();
        for(var i = 0; i < childs.Count; i++)
        {
            if(childs[i] is VehicleWheel)
            {
                var wheel = (VehicleWheel)childs[i];

                //GD.Print(wheel_broken.CanInstance());
                var wheelBroken = (RigidBody)wheel_broken.Instance();
                GetTree().GetRoot().AddChild(wheelBroken);
                wheelBroken.SetGlobalTransform(wheel.GlobalTransform);
                wheelBroken.ApplyImpulse(wheel.GlobalTransform.origin, this.GlobalTransform.origin - wheel.GlobalTransform.origin);

                // SUPER HACKY COLLISION FIX
                // DO NOT TOUCH!1 :P
                Spatial collisionShape = (Spatial)GetNode("CollisionShape");
                collisionShape.SetScale(collisionShape.Scale + new Vector3(0.0f, -1.0f, 0.0f));
                collisionShape.Translate(Vector3.Up * 1.0f);
                
                wheel.QueueFree();
            }
        }

	}

    public void GetRect(float damageInput)
    {
		
		
		score.IncreaseScore((int)damageInput * (int)damageInput);
		
        this.health -= damageInput;
        if(this.health < 0.0f)
        {
            this.Explode();
            this.Stun(500.0f);
        }
    }

    public override void _PhysicsProcess(float delta)
    {
        if(applyDamage)
        {
            float linearVelocityChange = ((GetLinearVelocity() - lastLinearVelocity) * Vector3.Up).Length();
            float angularVelocityChange = (GetAngularVelocity() - lastAngularVelocity).Length();
            float velocityChange = angularVelocityChange;
            if(velocityChange > 2.0f)
            {
                GetRect(velocityChange * 20.0f);
            }
            GD.Print(linearVelocityChange);
            GD.Print(angularVelocityChange);
            applyDamage = false;
            //damageParticleEffect.SetEmitting(false);
        }

        lastAngularVelocity = GetAngularVelocity();
        lastLinearVelocity = GetLinearVelocity();

        if(stunDuration > 0.0f)
        {
            Brake = 1.0f;
            EngineForce = 0.0f;
            return;
        }
        else
        {
            this.spotlight.SetVisible(true);
        }
            
        float playerDirection = (player.GlobalTransform.origin - this.GlobalTransform.origin).Dot(this.GlobalTransform.basis.x);

        if(playerDirection > 0.0f)
        {
            steer_target = STEER_LIMIT;
            //GD.Print("Steering Left");
        }
        else
        {
            steer_target = -STEER_LIMIT;
            //GD.Print("Steering Right");
        }

        Brake = 0.0f;

        if(steer_target < steer_angle)
        {
            steer_angle -= STEER_SPEED * delta;
            if(steer_target > steer_angle)
            {
                steer_angle = steer_target;
            }
        }
        else if(steer_target > steer_angle)
        {
            steer_angle += STEER_SPEED * delta;
            if(steer_target < steer_angle)
            {
                steer_angle = steer_target;
            }
        }

        Steering = steer_angle;
        EngineForce = engine_force_value;
    }

    public void Stun(float stunDuration)
    {
        this.stunDuration = stunDuration;
		this.spotlight.SetVisible(false);
    }

    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here

        this.player = (Spatial)GetNode("../../Truck/BODY");
		this.spotlight = (SpotLight)GetNode("SpotLight");
        this.damageParticleEffect = (Particles)GetNode("Particles/Particles");
		this.score = this.GetTree().GetRoot().GetNode("town_scene/Score") as Score;
		
		this.score.RegisterEnemy();
    }

    public override void _Process(float delta)
    {
        stunDuration -= delta;
    }

    private void _on_BODY_body_entered(Godot.Object body)
    {
        if(body is TRAILER)
        {
            applyDamage = true;
            damageParticleEffect.SetEmitting(true);
        }
        // Replace with function body
    }
}
