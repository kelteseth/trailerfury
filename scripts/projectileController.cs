using Godot;
using System;

public class projectileController : Spatial
{
    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";

    [Export]
    public bool useFixedVelocity = true;

    [Export]
    public float fixedVelocity = 10.0f;

    [Export]
    public float stunDuration = 4.0f;
	
	[Export]
	public float lifetime = 0.1f;

    private RigidBody rigidBody;
	

    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here
        
    }

    public Vector3 StartFlying(Vector3 targetPosition)
    {
        Vector3 velocity;
        rigidBody = ((RigidBody)GetNode("RigidBody"));

		if(!useFixedVelocity)
        {
            float time = (float)Math.Sqrt((this.GlobalTransform.origin.y * 2.0f) / 9.18f);
            Vector3 distance2D = targetPosition - this.GlobalTransform.origin;
            distance2D.y = 0.0f;

            velocity = (distance2D.Length() / time) * this.Transform.basis.x;
        }
        else
        {
            velocity = fixedVelocity * this.Transform.basis.x;
        }

        //((RigidBody)GetNode("RigidBody")).ApplyImpulse(targetPosition, targetPosition - this.GlobalTransform.origin);
        rigidBody.SetLinearVelocity((velocity + ((RigidBody)GetNode("../town_scene/Truck/TRAILER")).GetLinearVelocity()));
        return velocity;
    }

    public override void _Process(float delta)
    { 
        /*
        Godot.Array wtf = rigidBody.GetCollidingBodies();
        for(int i = 0; i < wtf.Count; i++)
        {
            GD.Print(wtf[i]);
        }
        */
		lifetime -= delta;
		if(lifetime < 0.0f)
			this.QueueFree();
    }
	private void _on_RigidBody_body_entered(Godot.Object body)
	{
        var a = body as enemyController;
        if(a != null)
		{
			var enemy = (enemyController)body;
            enemy.Stun(stunDuration);
		}
		
	    // Replace with function body
	}
}
