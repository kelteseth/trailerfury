using Godot;
using System;

public class Cannon : Spatial
{
    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";

	[Export]
    public PackedScene projectileScene;

    [Export]
    public float recoilScale;

    [Export]
    public float reloadTime = 0.5f;
    private float reloadValue = 0.0f;
    
    private AudioStreamPlayer3D cannonSound;
	private Particles cannonParticles;
	
	protected Spatial spawnpoint;

    public void startShooting(Vector3 targetVector)
    {
		
        if(reloadValue <= 0.0f)
        {
            this.cannonSound.Play();
            this.cannonParticles.SetEmitting(true);

            projectileController projectileRoot = (projectileController)projectileScene.Instance();
            this.GetTree().GetRoot().AddChild(projectileRoot);
            projectileRoot.SetGlobalTransform(spawnpoint.GlobalTransform);
            Vector3 recoilVelocity = projectileRoot.StartFlying(targetVector);
            ((RigidBody)this.GetNode("../../../TRAILER")).ApplyImpulse(this.GlobalTransform.origin, recoilVelocity * -recoilScale);
            reloadValue = reloadTime;
        }
    }

    public void endShooting(Vector3 targetVector)
    {
        
    }

    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here
        spawnpoint = (Spatial)GetNode("Muzzle");
		this.cannonSound = (AudioStreamPlayer3D)this.GetNode("Muzzle/Sound");
		this.cannonParticles = (Particles)this.GetNode("Muzzle/Particles/Particles");
    }

    public override void _Process(float delta)
    {
        reloadValue -= delta;
        //GD.Print(projectile.GlobalTransform.origin);GD.Print(projectile.GlobalTransform.origin);
    }
}
