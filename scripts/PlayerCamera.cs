using Godot;
using System;

public class PlayerCamera : Spatial
{
    protected Spatial trailer;
    protected Camera camera;
    protected Cannon cannon;
    public Vector3 mousePosition;

    const float Y_OFFSET = 15.0f;

    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here
        
        trailer = (Spatial)this.GetNode("../Truck/TRAILER");
        cannon = (Cannon)this.GetNode("../Truck/TRAILER/TRAILER/Cannon");
        camera = (Camera)this.GetNode("Camera");
    }

    public override void _Input(InputEvent @event)
    {
        // Mouse in viewport coordinates
        if (@event is InputEventMouseButton eventMouseButton)
        {
            if(eventMouseButton.Pressed)
                cannon.startShooting(mousePosition);
            else if(eventMouseButton.Pressed)
                cannon.endShooting(mousePosition);
        }
    }
    public float tmpVelocity;

    public override void _Process(float delta)
    {
		float trailerVelocity = ((RigidBody)trailer).LinearVelocity.Length();
		
        Vector3 newOrigin = new Vector3(trailer.GlobalTransform.origin.x, Y_OFFSET, trailer.GlobalTransform.origin.z);
        this.SetTranslation(newOrigin);
		this.LookAt(trailer.Transform.origin, Vector3.Up);

        this.tmpVelocity = Mathf.Lerp(tmpVelocity, trailerVelocity, 0.02f);
        this.Translate(new Vector3(0, tmpVelocity, 0));

		
		Vector3 mouseRayStart = camera.ProjectRayNormal(GetViewport().GetMousePosition());
        mousePosition = camera.GlobalTransform.origin - mouseRayStart * (camera.GlobalTransform.origin.y / mouseRayStart.y);
        cannon.LookAt(mousePosition, Vector3.Up);
        cannon.SetRotation(cannon.Rotation * Vector3.Up);
    }
}
