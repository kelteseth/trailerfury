using Godot;
using System;

public class trailer_truck : Spatial
{
    // Member variables here, example:
    // private int a = 2;
    // private string b = "textvar";wwwwwwwwwwwwwww
	
	RigidBody rigidBody;
	VehicleBody vehicleBody;
	AudioStreamPlayer audioPlayer0,audioPlayer1, audioPlayer2, audioPlayer3, audioPlayerLoop;
	float currentVelocity;

    enum VelocitySoundLevel
    {
        Init,
        Level0,
        Level1,
        Level2,
        Level3,
        LevelLoop,
    }

    VelocitySoundLevel level = VelocitySoundLevel.Init;


    public override void _Ready()
    {
        // Called every time the node is added to the scene.
        // Initialization here
		rigidBody   = GetNode("BODY") as RigidBody;
		vehicleBody = GetNode("BODY") as VehicleBody;

        // Somehow I cannot load the files for one audiostreamplayer...
        audioPlayer0 = GetNode("CarSound0") as AudioStreamPlayer;
        audioPlayer1 = GetNode("CarSound1") as AudioStreamPlayer;
        audioPlayer2 = GetNode("CarSound2") as AudioStreamPlayer;
        audioPlayer3 = GetNode("CarSound3") as AudioStreamPlayer;
        audioPlayerLoop = GetNode("CarSoundLoop") as AudioStreamPlayer;
        
    }
    public override void _Process(float delta)
    {

		currentVelocity = rigidBody.LinearVelocity.Length() ;
        GD.Print(level);
		if(currentVelocity > .1f && currentVelocity < 5.0f) {

                if(level != VelocitySoundLevel.Level0) {
                    GD.Print(level);
                    level = VelocitySoundLevel.Level0;
					audioPlayer0.Play();
                    audioPlayer1.Stop();
                    audioPlayer2.Stop();
                    audioPlayer3.Stop();
                    audioPlayerLoop.Stop();
                }
        } else  if(currentVelocity >= 5.0f && currentVelocity < 10.0f) {
            
                if(level != VelocitySoundLevel.Level1) {
                    level = VelocitySoundLevel.Level1;
                    audioPlayer0.Stop();
                    audioPlayer1.Play();
                    audioPlayer2.Stop();
                    audioPlayer3.Stop();
                    audioPlayerLoop.Stop();
                }
        } else  if(currentVelocity >= 10.0f && currentVelocity < 15.0f) {
            
                if(level != VelocitySoundLevel.Level2) {
                    level = VelocitySoundLevel.Level2;
                    audioPlayer0.Stop();
                    audioPlayer1.Stop();
                    audioPlayer2.Play();
                    audioPlayer3.Stop();
                    audioPlayerLoop.Stop();
                }
        } else  if(currentVelocity >= 15.0f && currentVelocity < 25.0f) {
            
                if(level != VelocitySoundLevel.Level3) {
                    level = VelocitySoundLevel.Level3;
                    audioPlayer0.Stop();
                    audioPlayer1.Stop();
                    audioPlayer2.Stop();
                    audioPlayer3.Play();
                    audioPlayerLoop.Stop();
                }
        } else  if(currentVelocity >= 25.0f) {
            
                if(level != VelocitySoundLevel.LevelLoop) {
                    level = VelocitySoundLevel.LevelLoop;
                    audioPlayer0.Stop();
                    audioPlayer1.Stop();
                    audioPlayer2.Stop();
                    audioPlayer3.Stop();
                    audioPlayerLoop.Play();
                }
        }
		
		
    }
}
