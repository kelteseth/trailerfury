extends VehicleBody

# Member variables
const STEER_SPEED = 1.5
const STEER_LIMIT = 0.8

const IDLE_PITCH = 0.75
const POWER_PITCH = 2.0

var steer_angle = 0
var steer_target = 0

export var engine_force_value = 40

var tmpPitch = IDLE_PITCH
var targetPitch = IDLE_PITCH
var engineSound

func _ready():
	self.engineSound = get_node("EngineSound")

func _physics_process(delta):
	if Input.is_action_pressed("ui_left"):
		steer_target = STEER_LIMIT
	elif Input.is_action_pressed("ui_right"):
		steer_target = -STEER_LIMIT
	else:
		steer_target = 0
	
	if Input.is_action_pressed("ui_up"):
		engine_force = engine_force_value
		targetPitch = POWER_PITCH
	else:
		engine_force = 0
		targetPitch = IDLE_PITCH
	
	if Input.is_action_pressed("ui_down"):
		engine_force = -engine_force_value

	
	if steer_target < steer_angle:
		steer_angle -= STEER_SPEED * delta
		if steer_target > steer_angle:
			steer_angle = steer_target
	elif steer_target > steer_angle:
		steer_angle += STEER_SPEED * delta
		if steer_target < steer_angle:
			steer_angle = steer_target
	
	steering = steer_angle
	
	self.tmpPitch = lerp(self.tmpPitch, targetPitch, 0.03)
	self.engineSound.pitch_scale = self.tmpPitch
