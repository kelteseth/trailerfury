using Godot;
using System;

public class Score : Control
{
	private int currentScore;
	private float tmpScore;
	private int enemyCount;
	private int totalEnemyCount;

	private Label pointsLabel;
	private Label enemyCountLabel;


    public override void _Ready() {
		this.pointsLabel = (Label)this.GetNode("./Container/HBoxContainer/VBoxScore/Score");
		this.enemyCountLabel = (Label)this.GetNode("./Container/HBoxContainer/VBoxEnemyCount/EnemyCount");
	}

	public void RegisterEnemy() {
		this.totalEnemyCount++;	
		this.enemyCount = this.totalEnemyCount;
	}

	public void IncreaseScore(int points) {
		this.currentScore += points;
	}

	public void DecreaseScore(int points) {
		this.currentScore -= points;
	}
	
	public void IncreaseEnemyCount(int amount) {
		this.enemyCount += amount;
	}
	
	public void DecreaseEnemyCount() {
		this.enemyCount--;

		if (this.enemyCount == 0) {
			this.ShowWinningScreen();
		}
	}

    public override void _Process(float delta) {
		this.tmpScore = Mathf.Lerp(this.tmpScore, this.currentScore, 0.1f);
		this.pointsLabel.Text = Mathf.Round(this.tmpScore).ToString();
		
		this.enemyCountLabel.Text = this.enemyCount.ToString() + " / " + this.totalEnemyCount.ToString();
	}

	private void ShowWinningScreen() {
		WinLoose winLoose = this.GetTree().GetRoot().GetNode("town_scene/WinLoose") as WinLoose;
		winLoose.DisplayWinScore(this.currentScore);
		this.SetVisible(false);
	}
}
