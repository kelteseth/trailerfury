using Godot;
using System;

public class WinLoose : Control
{
    Label scoreLabel;

    public override void _Ready()
    {
        this.scoreLabel = this.GetNode("WinContainer/CenterContainer/VBoxContainer/PointsLabel") as Label;
    }
    public override void _Process(float delta)
    {

    }

    public void DisplayWinScore(int score) {
        this.SetVisible(true);
        this.scoreLabel.Text = score.ToString() + " Points";
		(this.GetNode("Explosion") as AudioStreamPlayer).Play();
    }
}
