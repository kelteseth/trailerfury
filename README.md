<div>
<img width="100%" height="300" src="https://screen-play.app/images/trailerfury_logo_gitlab_fullwidth.svg">
</div>

<div align="center">
What does Home mean to you? Well that's an easy question if only one home is left. Your trailer is the last of its kind and you must defend with your life. 

Beware the canon on your trailer can only stun your foes for a short time. You only can deal damage with your trailer! The harder you kick the higher you score will be.  

</div>
<br>

# Getting started

### Requirements
1. Git
2. Godot 3.0.6 Mono version (C# support)

``` bash
# HTTPS
git clone https://gitlab.com/kelteseth/trailerfury.git
```

### Credits
 - [MrMinimal](https://www.reddit.com/user/mrminimal) - Coding
 - [JoeyGartain](https://www.reddit.com/user/JoeyGartain) - Artist
 - [stupixion](https://www.reddit.com/user/stupixion) - Coding
 - [Kelteseth](https://www.reddit.com/user/kelteseth) - A bit of everything (mostly resolving merge conflicts)
 - Anonymes Eichhörnchen - Coding

### Assets
 - [flaticon](https://www.flaticon.com/)
 - [freesound](https://freesound.org/)
 - [youtube.com/audiolibrary](https://www.youtube.com/audiolibrary/soundeffects)
